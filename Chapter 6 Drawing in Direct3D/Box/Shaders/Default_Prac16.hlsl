//***************************************************************************************
// color.hlsl by Frank Luna (C) 2015 All Rights Reserved.
//
// Transforms and colors geometry.
//***************************************************************************************

cbuffer cbPerObject : register(b0)
{
	float4x4 gWorldViewProj;
};

struct VertexIn
{
	float3 PosL  : POSITION;
	float4 Color : COLOR;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
	float4 Color : COLOR;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	// local space에서 동차 절단 공간(homogeneous clip space)으로 변환한다
	vout.PosH = mul(float4(vin.PosL, 1.0f), gWorldViewProj);

	// 꼭짓점 색은 바꿀 필요 없으므로 그대로 넘긴다
	vout.Color = vin.Color;

	return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
	const float pi = 3.14159;

	// set color
	//pin.Color.r *= sin(gTime);
	//pin.Color.g *= cos(gTime);

	float s = 0.5f * sin(2 * gTime - 0.25f * pi) + 0.5f;

	float4 c = lerp(pin.Color, gPulseColor, s);
	return c;

	//clip(pin.Color.r - 0.5f);
	//return pin.Color;
}