#include "Common.hlsli"

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC : TEXCOORD;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosL : POSITION;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	// 
	vout.PosL = vin.PosL;

	// local space를 world space로 전환
	float4 posW = mul(float4(vin.PosL, 1.0f), gWorld);

	// 항상 카메라가 sky의 중심이어야 함
	// (카메라에 대해 배경이 구형으로 둘러싼 느낌)
	posW.xyz += gEyePosW;

	// z/w = 1이 되도록 z = w로 만들어야 함 (skydome은 언제나 시야 최대치에 있어야함)
	vout.PosH = mul(posW, gViewProj).xyww;

	return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
	return gCubeMap.Sample(gsamLinearWrap, pin.PosL);
}