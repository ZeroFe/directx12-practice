#include "Common.hlsli"

#define NUM_CONTROL_POINTS 4

struct VertexIn
{
	float3 PosL : POSITION;
};

struct VertexOut
{
	float3 PosL : POSITION;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;

	vout.PosL = vin.PosL;

	return vout;
}

static const float tessFactor = 32.0f;

struct PatchTess
{
	float EdgeTess[4]			: SV_TessFactor;
	float InsideTess[2]			: SV_InsideTessFactor;
};

PatchTess ConstantHS(
	InputPatch<VertexOut, 4> patch,
	uint PatchID : SV_PrimitiveID)
{
	PatchTess pt;

	pt.EdgeTess[0] = tessFactor;
	pt.EdgeTess[1] = tessFactor;
	pt.EdgeTess[2] = tessFactor;
	pt.EdgeTess[3] = tessFactor;

	pt.InsideTess[0] = tessFactor;
	pt.InsideTess[1] = tessFactor;

	return pt;
}

struct HullOut
{
	float3 PosL : POSITION;
};

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("ConstantHS")]
[maxtessfactor(64.0f)]
HullOut HS(
	InputPatch<VertexOut, 4> p,
	uint i : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID)
{
	HullOut Output;

	Output.PosL = p[i].PosL;

	return Output;
}

static const float HeightFactor0 = 0.5f;
static const float HeightFactor1 = 1.0f;

float GetDomainY(float2 uv)
{
	MaterialData matData = gMaterialData[gMaterialIndex];

	// height map을 통해 정점의 높이를 구한다
	// height map은 alpha 값이 높이에 해당함
	float4 texC = mul(float4(uv, 0.0f, 1.0f), gTexTransform);
	float2 height0Coord = mul(texC, gNormalHeightTransform0).xy;
	float2 height1Coord = mul(texC, gNormalHeightTransform1).xy;
	float y0 = gTextureMaps[matData.NormalMapIndex].SampleLevel(gsamLinearWrap, height0Coord, 0).a;
	float y1 = gTextureMaps[matData.NormalMapIndex2].SampleLevel(gsamLinearWrap, height1Coord, 0).a;
	return y0 * HeightFactor0 + y1 * HeightFactor1;
}

float3 GetDomainPosition(float2 uv, float3 basePos, float xSize, float zSize)
{
	return float3(basePos.x + uv.x * xSize, GetDomainY(uv), basePos.z + uv.y * zSize);
}

struct DomainOut
{
	float4 PosH : SV_POSITION;
	float3 PosW    : POSITION;
	float3 NormalW : NORMAL;
	float3 TangentW : TANGENT;
	float2 TexC    : TEXCOORD;
};

[domain("quad")]
DomainOut DS(PatchTess patchTess,
	float2 uv : SV_DomainLocation,
	const OutputPatch<HullOut, 4> bezPatch)
{
	DomainOut dout;

	// 정점 위치 구하기
	float3 basePos = bezPatch[0].PosL;
	float xSize = bezPatch[1].PosL.x - bezPatch[0].PosL.x;
	float zSize = bezPatch[2].PosL.z - bezPatch[0].PosL.z;
	float3 p = GetDomainPosition(uv, basePos, xSize, zSize);

	float4 posW = mul(float4(p, 1.0f), gWorld);

	/*
	 * 노말 벡터와 탄젠트 벡터는 파도 공식(Waves 클래스에 나온)을 이용해서 만들어낸다
	 * ㅁ상ㅁ
	 * 좌ㅇ우
	 * ㅁ하ㅁ
	 * 해당 정점의 상하좌우 4개의 인접 정점의 높이들로 만든 벡터들을 외적하면 노말벡터를 만들 수 있다
	 * ex) cross(상 - 하, 좌 - 우)
	 * 또한 탄젠트 벡터는 해당 평면 위의, 노말 벡터와 수직인 벡터이므로 좌우 벡터를 통해 구할 수 있다
	 */
	float tu = 1.0f / tessFactor;
	float tv = 1.0f / tessFactor;
	// uv는 0~1 사이의 값이므로 주변 정점의 좌표는 [0,1]로 제한되어야 한다
	// 만약 [0,1] 값 제한으로 인해 상/하/좌/우 정점의 uv가 해당 정점과 같더라도 큰 상관없다
	// 왜냐면 상하, 좌우의 좌표가 겹치는 일은 없기 때문에 상하좌우 좌표를 통해 평면을 구할 수 있기 때문이다
	float2 uvTop = float2(uv.x, saturate(uv.y - tv));
	float2 uvBottom = float2(uv.x, saturate(uv.y + tv));;
	float2 uvLeft = float2(saturate(uv.x - tu), uv.y);
	float2 uvRight = float2(saturate(uv.x + tu), uv.y);
	float3 posTop = GetDomainPosition(uvTop, basePos, xSize, zSize);
	float3 posBottom = GetDomainPosition(uvBottom, basePos, xSize, zSize);
	float3 posLeft = GetDomainPosition(uvLeft, basePos, xSize, zSize);
	float3 posRight = GetDomainPosition(uvRight, basePos, xSize, zSize);

	float3 normalL = normalize(cross(posBottom - posTop, posLeft - posRight));
	float3 tangentL = normalize(posLeft - posRight);

	dout.PosH = mul(posW, gViewProj);
	dout.PosW = posW.xyz;
	dout.NormalW = mul(float4(normalL, 1.0f), gWorld).xyz;
	dout.TangentW = mul(float4(tangentL, 1.0f), gWorld).xyz;

	// 배열에서 material data를 가져온다
	MaterialData matData = gMaterialData[gMaterialIndex];
	float4 texC = mul(float4(uv, 0.0f, 1.0f), gTexTransform);
	dout.TexC = mul(texC, matData.MatTransform).xy;

	return dout;
}

float4 PS(DomainOut pin) : SV_Target
{
	// material data 가져오기
	MaterialData matData = gMaterialData[gMaterialIndex];
	float4 diffuseAlbedo = matData.DiffuseAlbedo;
	float3 fresnelR0 = matData.FresnelR0;
	float roughness = matData.Roughness;
	uint diffuseTexIndex = matData.DiffuseMapIndex;
	uint normalMapIndex0 = matData.NormalMapIndex;
	uint normalMapIndex1 = matData.NormalMapIndex2;

	// normal 벡터를 보간하면 unnormalize해지므로 다시 normalize한다
	pin.NormalW = normalize(pin.NormalW);

	float3 normalMapSample0 = gTextureMaps[normalMapIndex0].Sample(gsamAnisotropicWrap, pin.TexC).rgb;
	float3 bumpedNormalW0 = NormalSampleToWorldSpace(normalMapSample0.rgb, pin.NormalW, pin.TangentW);

	float3 normalMapSample1 = gTextureMaps[normalMapIndex1].Sample(gsamAnisotropicWrap, pin.TexC).rgb;
	float3 bumpedNormalW1 = NormalSampleToWorldSpace(normalMapSample1.rgb, pin.NormalW, pin.TangentW);

	float3 bumpedNormalW = normalize(bumpedNormalW0 + bumpedNormalW1);

	// texture array에서 값을 찾는다
	diffuseAlbedo *= gTextureMaps[diffuseTexIndex].Sample(gsamLinearWrap, pin.TexC);

	// normal을 보간하면 유닛 벡터가 아니게 되므로 다시 normalize
	pin.NormalW = normalize(pin.NormalW);

	float3 toEyeW = normalize(gEyePosW - pin.PosW);

	float4 ambient = gAmbientLight * diffuseAlbedo;

	const float shininess = 1.0f - roughness;
	Material mat = { diffuseAlbedo, fresnelR0, shininess };
	float3 shadowFactor = 1.0f;
	float4 directLight = ComputeLighting(gLights, mat, pin.PosW, bumpedNormalW, toEyeW, shadowFactor);

	float4 litColor = ambient + directLight;

	// specular reflection 추가
	float3 r = reflect(-toEyeW, bumpedNormalW);
	float4 reflectionColor = gCubeMap.Sample(gsamLinearWrap, r);
	float3 fresnelFactor = SchlickFresnel(fresnelR0, bumpedNormalW, r);
	litColor.rgb += shininess * fresnelFactor * reflectionColor.rgb;

	litColor.a = diffuseAlbedo.a;

	return litColor;
}