#include "Common.hlsli"

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC : TEXCOORD;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float2 TexC : TEXCOORD;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;

	// 壕伸拭辞 material data研 亜閃紳陥
	MaterialData matData = gMaterialData[gMaterialIndex];

	// local space拭辞 world space稽 痕発
	float4 posW = mul(float4(vin.PosL, 1.0f), gWorld);
	vout.PosW = posW.xyz;

	// non-uniform scaling戚虞 亜舛; 益係走 省生檎 world matrix税 inverse-transpose研 紫遂背醤 敗
	vout.NormalW = mul(vin.NormalL, (float3x3)gWorld);

	// homogeneous clip space稽 痕発
	vout.PosH = mul(posW, gViewProj);

	// 
	float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gTexTransform);
	vout.TexC = mul(texC, matData.MatTransform).xy;

	return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
	// material data 亜閃神奄
	MaterialData matData = gMaterialData[gMaterialIndex];
	float4 diffuseAlbedo = matData.DiffuseAlbedo;
	float3 fresnelR0 = matData.FresnelR0;
	float roughness = matData.Roughness;
	uint diffuseTexIndex = matData.DiffuseMapIndex;

	// texture array拭辞 葵聖 達澗陥
	diffuseAlbedo *= gDiffuseMap[diffuseTexIndex].Sample(gsamLinearWrap, pin.TexC);

	// normal聖 左娃馬檎 政間 困斗亜 焼艦惟 鞠糠稽 陥獣 normalize
	pin.NormalW = normalize(pin.NormalW);

	float3 toEyeW = normalize(gEyePosW - pin.PosW);

	float4 ambient = gAmbientLight * diffuseAlbedo;

	const float shininess = 1.0f - roughness;
	Material mat = { diffuseAlbedo, fresnelR0, shininess };
	float3 shadowFactor = 1.0f;
	float4 directLight = ComputeLighting(gLights, mat, pin.PosW, pin.NormalW, toEyeW, shadowFactor);

	float4 litColor = ambient + directLight;

	// [BeginAddContext]
	// 厳 幻級嬢辞 設 照 宜焼姶
	/*
	 * 鷺君 因縦 企中 幻糾 : 叔薦 因縦精 蟹掻拭 達焼左澗杏稽
	 * しけし
	 * けぞけ
	 * しけし
	 * 戚係惟 児巴聖 嗣焼辞 鷺君 旋遂
	 * ぞ : 1 - 2 * Roughness - 2 * Roughness^2
	 * け : 0.5 * Roughness
	 * し : 0.5 * Roughness^2
	 * 戚係惟 馬檎 杯 1 鞠艦猿 宜焼澗 哀牛?
	 */
	float value[9];
	float roughSquare = roughness * roughness;
	value[0] = 0.5 * roughSquare;
	value[2] = 0.5 * roughSquare;
	value[6] = 0.5 * roughSquare;
	value[8] = 0.5 * roughSquare;

	value[1] = 0.5 * roughness;
	value[3] = 0.5 * roughness;
	value[5] = 0.5 * roughness;
	value[7] = 0.5 * roughness;

	value[4] = 1 - 2 * roughness - 2 * roughSquare;

	// r精 爽痕 発井聖 2託据生稽 嗣焼辞 幻糾
	float3 r[9];
	float3 origin = reflect(-toEyeW, pin.NormalW);
	float3 y = normalize(cross(-toEyeW, origin));
	float3 x = normalize(cross(y, origin));

	const float size = 2.0f;
	float4 reflectionColor[9];
	float3 fresnelFactor[9];
	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		r[i] = origin + size * ((i / 3) - 1) * x + size * ((i % 3) - 1) * y;
		reflectionColor[i] = gCubeMap.Sample(gsamLinearWrap, r[i]);
		fresnelFactor[i] = SchlickFresnel(fresnelR0, pin.NormalW, r[i]);
		litColor.rgb += shininess * value[i] * fresnelFactor[i] * reflectionColor[i].rgb;
	}
	// [EndAddContext]

	litColor.a = diffuseAlbedo.a;

	return litColor;
}