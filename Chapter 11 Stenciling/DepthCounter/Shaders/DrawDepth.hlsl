cbuffer cbColor : register(b3)
{
	float gColorRed;
	float gColorBlue;
	float gColorGreen;
	float gColorAlpha;
}

// Quad(직사각형)의 Vertex index에 대응되는 Texture Coordinate 배열
static const float2 gTexCoords[6] =
{
	float2(0.0f, 1.0f),		// 0
	float2(0.0f, 0.0f),		// 1
	float2(1.0f, 0.0f),		// 2
	float2(0.0f, 1.0f),		// 0
	float2(1.0f, 0.0f),		// 2
	float2(1.0f, 1.0f)		// 3
};

static const float4 ColorBlack = { 0.0f, 0.0f, 0.0f, 1.0f };
static const float4 ColorRed = {1.0f, 0.0f, 0.0f, 1.0f};
static const float4 ColorBlue = {0.0f, 1.0f, 0.0f, 1.0f};
static const float4 ColorGreen = {0.0f, 0.0f, 1.0f, 1.0f};
static const float4 ColorCyan = { 0.0f, 1.0f, 1.0f, 1.0f };
static const float4 ColorMagenta = { 1.0f, 1.0f, 0.0f, 1.0f };
static const float4 ColorYellow = { 1.0f, 0.0f, 1.0f, 1.0f };
static const float4 ColorWhite = { 1.0f, 1.0f, 1.0f, 1.0f };

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float2 TexC : TEXCOORD;
};

VertexOut VS(uint vid : SV_VertexID)
{
	VertexOut vout;

	vout.TexC = gTexCoords[vid];

	// [0,1]^2를 NDC 공간([-1,-1]^3, w)으로 사상한다
	vout.PosH = float4(2.0f * vout.TexC.x - 1.0f, 1.0f - 2.0f * vout.TexC.y, 0.0f, 1.0f);

	return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
	float4 color = {gColorRed, gColorBlue, gColorGreen, gColorAlpha};
	return color;
}