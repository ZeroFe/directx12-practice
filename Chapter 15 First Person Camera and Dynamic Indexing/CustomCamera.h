#ifndef CUSTOMCAMERA_H
#define CUSTOMCAMERA_H

#include "../../Common/d3dUtil.h"

/**
 * \brief Custom Camera는 기존 Camera 클래스에 Roll, ZoomIn, ZoomOut을 추가한 클래스
 */
class CustomCamera
{
public:
	CustomCamera();
	~CustomCamera();

	// Get/Set world camera position.
	DirectX::XMVECTOR GetPosition() const;
	DirectX::XMFLOAT3 GetPosition3f() const;
	void SetPosition(float x, float y, float z);
	void SetPosition(const DirectX::XMFLOAT3& v);

	// Get camera basis vectors.
	DirectX::XMVECTOR GetRight() const;
	DirectX::XMFLOAT3 GetRight3f() const;
	DirectX::XMVECTOR GetUp() const;
	DirectX::XMFLOAT3 GetUp3f() const;
	DirectX::XMVECTOR GetLook() const;
	DirectX::XMFLOAT3 GetLook3f() const;

	// Get frustum properties.
	float GetNearZ() const;
	float GetFarZ() const;
	float GetAspect() const;
	float GetFovY() const;
	float GetFovX() const;

	// Get near and far plane dimensions in view space coordinates.
	float GetNearWindowWidth() const;
	float GetNearWindowHeight() const;
	float GetFarWindowWidth() const;
	float GetFarWindowHeight() const;

	// Set frustum.
	void SetLens(float fovY, float aspect, float zn, float zf);

	void LookAt(DirectX::FXMVECTOR pos, DirectX::FXMVECTOR target, DirectX::FXMVECTOR worldUp);
	void LookAt(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& target, const DirectX::XMFLOAT3& up);

	DirectX::XMMATRIX GetView() const;
	DirectX::XMMATRIX GetProj() const;

	DirectX::XMFLOAT4X4 GetView4x4f() const;
	DirectX::XMFLOAT4X4 GetProj4x4f() const;

	// d(distance)만큼 카메라 이동
	void Strafe(float d);	// X축 이동
	void Walk(float d);		// Z축 이동

	// 카메라 회전
	void Pitch(float angle);	//
	void RotateY(float angle);	//
	void Roll(float angle);		// 시선 벡터를 기준으로 회전한다

	// 카메라 줌인 / 줌아웃
	void ZoomIn(float subtractedFovY);
	void ZoomOut(float addedFovY);

	// 카메라 위치/각도 변경 시 view matrix를 다시 만든다
	void UpdateViewMatrix();

private:
	// 카메라가 가지고 있는 자체 좌표계 (world space랑 상대적)
	DirectX::XMFLOAT3 mPosition = { 0.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT3 mRight = { 1.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT3 mUp = { 0.0f, 1.0f, 0.0f };
	DirectX::XMFLOAT3 mLook = { 0.0f, 0.0f, 1.0f };

	// frustum 속성 저장 변수
	float mNearZ = 0.0f;
	float mFarZ = 0.0f;
	float mAspect = 0.0f;
	float mFovY = 0.0f;
	float mNearWindowHeight = 0.0f;
	float mFarWindowHeight = 0.0f;

	bool mViewDirty = true;

	// View / Proj 행렬
	DirectX::XMFLOAT4X4 mView = MathHelper::Identity4x4();
	DirectX::XMFLOAT4X4 mProj = MathHelper::Identity4x4();
};

#endif
