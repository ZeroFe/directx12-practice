#include "CustomCamera.h"

using namespace DirectX;

CustomCamera::CustomCamera()
{
	SetLens(0.25f * MathHelper::Pi, 1.0f, 1.0f, 1000.0f);
}

CustomCamera::~CustomCamera()
{

}

DirectX::XMVECTOR CustomCamera::GetPosition() const
{
	return XMLoadFloat3(&mPosition);
}

DirectX::XMFLOAT3 CustomCamera::GetPosition3f() const
{
	return mPosition;
}

void CustomCamera::SetPosition(float x, float y, float z)
{
	mPosition = XMFLOAT3(x, y, z);
	mViewDirty = true;
}

void CustomCamera::SetPosition(const DirectX::XMFLOAT3& v)
{
	mPosition = v;
	mViewDirty = true;
}

DirectX::XMVECTOR CustomCamera::GetRight() const
{
	return XMLoadFloat3(&mRight);
}

DirectX::XMFLOAT3 CustomCamera::GetRight3f() const
{
	return mRight;
}

DirectX::XMVECTOR CustomCamera::GetUp() const
{
	return XMLoadFloat3(&mUp);
}

DirectX::XMFLOAT3 CustomCamera::GetUp3f() const
{
	return mUp;
}

DirectX::XMVECTOR CustomCamera::GetLook() const
{
	return XMLoadFloat3(&mLook);
}

DirectX::XMFLOAT3 CustomCamera::GetLook3f() const
{
	return mLook;
}

float CustomCamera::GetNearZ() const
{
	return mNearZ;
}

float CustomCamera::GetFarZ() const
{
	return mFarZ;
}

float CustomCamera::GetAspect() const
{
	return mAspect;
}

float CustomCamera::GetFovY() const
{
	return mFovY;
}

float CustomCamera::GetFovX() const
{
	float halfWidth = 0.5f * GetNearWindowWidth();
	return 2.0f * atan(halfWidth / mNearZ);
}

float CustomCamera::GetNearWindowWidth() const
{
	return mAspect * mNearWindowHeight;
}

float CustomCamera::GetNearWindowHeight() const
{
	return mNearWindowHeight;
}

float CustomCamera::GetFarWindowWidth() const
{
	return mAspect * mFarWindowHeight;
}

float CustomCamera::GetFarWindowHeight() const
{
	return mFarWindowHeight;
}

void CustomCamera::SetLens(float fovY, float aspect, float zn, float zf)
{
	mFovY = fovY;
	mAspect = aspect;
	mNearZ = zn;
	mFarZ = zf;

	mNearWindowHeight = 2.0f * mNearZ * tanf(0.5f * mFovY);
	mFarWindowHeight = 2.0f * mFarZ * tanf(0.5f * mFovY);

	XMMATRIX P = XMMatrixPerspectiveFovLH(mFovY, mAspect, mNearZ, mFarZ);
	XMStoreFloat4x4(&mProj, P);
}

void CustomCamera::LookAt(DirectX::FXMVECTOR pos, DirectX::FXMVECTOR target, DirectX::FXMVECTOR worldUp)
{
	// 각각 look, right, up (카메라 기준)
	XMVECTOR L = XMVector3Normalize(XMVectorSubtract(target, pos));
	XMVECTOR R = XMVector3Normalize(XMVector3Cross(worldUp, L));
	XMVECTOR U = XMVector3Cross(L, R);

	XMStoreFloat3(&mPosition, pos);
	XMStoreFloat3(&mLook, L);
	XMStoreFloat3(&mRight, R);
	XMStoreFloat3(&mUp, U);

	mViewDirty = true;
}

void CustomCamera::LookAt(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& target, const DirectX::XMFLOAT3& up)
{
	XMVECTOR P = XMLoadFloat3(&pos);
	XMVECTOR T = XMLoadFloat3(&target);
	XMVECTOR U = XMLoadFloat3(&up);

	LookAt(P, T, U);

	mViewDirty = true;
}

DirectX::XMMATRIX CustomCamera::GetView() const
{
	assert(!mViewDirty);
	return XMLoadFloat4x4(&mView);
}

DirectX::XMFLOAT4X4 CustomCamera::GetView4x4f() const
{
	assert(!mViewDirty);
	return mView;
}

DirectX::XMMATRIX CustomCamera::GetProj() const
{
	return XMLoadFloat4x4(&mProj);
}

DirectX::XMFLOAT4X4 CustomCamera::GetProj4x4f() const
{
	return mProj;
}

void CustomCamera::Strafe(float d)
{
	// mPosition += d*mRight
	XMVECTOR s = XMVectorReplicate(d);
	XMVECTOR r = XMLoadFloat3(&mRight);
	XMVECTOR p = XMLoadFloat3(&mPosition);
	XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, r, p));

	mViewDirty = true;
}

void CustomCamera::Walk(float d)
{
	// mPosition += d*mLook
	XMVECTOR s = XMVectorReplicate(d);
	XMVECTOR l = XMLoadFloat3(&mLook);
	XMVECTOR p = XMLoadFloat3(&mPosition);
	XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, l, p));

	mViewDirty = true;
}

void CustomCamera::Pitch(float angle)
{
	//
	XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&mRight), angle);

	XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
	XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));

	mViewDirty = true;
}

void CustomCamera::RotateY(float angle)
{
	XMMATRIX R = XMMatrixRotationY(angle);

	XMStoreFloat3(&mRight, XMVector3TransformNormal(XMLoadFloat3(&mRight), R));
	XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
	XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));

	mViewDirty = true;
}

void CustomCamera::Roll(float angle)
{
	auto look = XMLoadFloat3(&mLook);
	XMMATRIX R = XMMatrixRotationAxis(look, angle);

	XMStoreFloat3(&mRight, XMVector3TransformNormal(XMLoadFloat3(&mRight), R));
	XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));

	mViewDirty = true;
}

void CustomCamera::ZoomIn(float subtractedFovY)
{
	float newFovY = mFovY - subtractedFovY;
	SetLens(newFovY, mAspect, mNearZ, mFarZ);
}

void CustomCamera::ZoomOut(float addedFovY)
{
	float newFovY = mFovY + addedFovY;
	SetLens(newFovY, mAspect, mNearZ, mFarZ);
}

void CustomCamera::UpdateViewMatrix()
{
	if (mViewDirty)
	{
		XMVECTOR R = XMLoadFloat3(&mRight);
		XMVECTOR U = XMLoadFloat3(&mUp);
		XMVECTOR L = XMLoadFloat3(&mLook);
		XMVECTOR P = XMLoadFloat3(&mPosition);

		// 회전 등을 수행하는 과정에서 L, U 등이 unit vector가 아니게 될 수 있음
		// 카메라 축을 단위 벡터에 맞게 normalize 한다
		L = XMVector3Normalize(L);
		U = XMVector3Normalize(U);

		// U, L은 이미 unit vector이므로, R은 외적만 하면 된다
		R = XMVector3Cross(U, L);

		// view matrix 값을 채운다
		float x = -XMVectorGetX(XMVector3Dot(P, R));
		float y = -XMVectorGetX(XMVector3Dot(P, U));
		float z = -XMVectorGetX(XMVector3Dot(P, L));

		XMStoreFloat3(&mRight, R);
		XMStoreFloat3(&mUp, U);
		XMStoreFloat3(&mLook, L);

		mView(0, 0) = mRight.x;
		mView(1, 0) = mRight.y;
		mView(2, 0) = mRight.z;
		mView(3, 0) = x;

		mView(0, 1) = mUp.x;
		mView(1, 1) = mUp.y;
		mView(2, 1) = mUp.z;
		mView(3, 1) = y;

		mView(0, 2) = mLook.x;
		mView(1, 2) = mLook.y;
		mView(2, 2) = mLook.z;
		mView(3, 2) = z;

		mView(0, 3) = 0.0f;
		mView(1, 3) = 0.0f;
		mView(2, 3) = 0.0f;
		mView(3, 3) = 1.0f;

		mViewDirty = false;
	}
}
