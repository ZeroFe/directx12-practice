#ifndef NUM_DIR_LIGHTS
#define NUM_DIR_LIGHTS 3
#endif

#ifndef NUM_POINT_LIGHTS
#define NUM_POINT_LIGHTS 0
#endif

#ifndef NUM_SPOT_LIGHTS
#define NUM_SPOT_LIGHTS 0
#endif

#include "LightingUtil.hlsli"

Texture2D    gDiffuseMap : register(t0);

SamplerState gsamPointWrap : register(s0);
SamplerState gsamPointClamp : register(s1);
SamplerState gsamLinearWrap : register(s2);
SamplerState gsamLinearClamp : register(s3);
SamplerState gsamAnisotropicWrap : register(s4);
SamplerState gsamAnisotropicClamp : register(s5);

cbuffer cbPerObject : register(b0)
{
	float4x4 gWorld;
	float4x4 gTexTransform;
};

cbuffer cbPass : register(b1)
{
	float4x4 gView;
	float4x4 gInvView;
	float4x4 gProj;
	float4x4 gInvProj;
	float4x4 gViewProj;
	float4x4 gInvViewProj;
	float3 gEyePosW;
	float cbPerObjectPad1;
	float2 gRenderTargetSize;
	float2 gInvRenderTargetSize;
	float gNearZ;
	float gFarZ;
	float gTotalTime;
	float gDeltaTime;
	float4 gAmbientLight;

	float4 gFogColor;
	float gFogStart;
	float gFogRange;
	float2 cbPerObjectPad2;

	Light gLights[MaxLights];
};

cbuffer cbMaterial : register(b2)
{
	float4 gDiffuseAlbedo;
	float3 gFresnelR0;
	float gRoughness;
	float4x4 gMatTransform;
};

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC : TEXCOORD;
};

struct VertexOut
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC : TEXCOORD;
};

struct GeoOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float2 TexC : TEXCOORD;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	vout.PosL = vin.PosL;
	vout.NormalL = vin.NormalL;
	vout.TexC = vin.TexC;

	return vout;
}

[maxvertexcount(3)]
void GS(triangle VertexOut gin[3],
	uint primID : SV_PrimitiveID,
	inout TriangleStream<GeoOut> triStream)
{
	// 삼각형의 세 정점으로부터 삼각형의 Normal Vector를 구한다
	// 삼각형의 Normal Vector = (세 정점의 Normal Vector의 합) / 3
	float3 triNormalL = (gin[0].NormalL + gin[1].NormalL + gin[2].NormalL) / 3;

	// 삼각형의 세 정점을 삼각형의 Normal Vector에 맞춰 이동시킨다
	[unroll]
	for (int i = 0; i < 3; ++i)
	{
		gin[i].PosL = gin[i].PosL + gTotalTime * triNormalL;
	}

	// 각 정점을 세계 행렬 및 동차 절단 공간에 맞게 변화시킨다
	GeoOut gout[3];
	[unroll]
	for (int j = 0; j < 3; ++j)
	{
		gout[j].PosW = mul(float4(gin[j].PosL, 1.0f), gWorld).xyz;
		gout[j].PosH = mul(float4(gout[j].PosW, 1.0f), gViewProj);
		gout[j].NormalW = mul(float4(gout[j].NormalW, 1.0f), gWorld).xyz;
		gout[j].TexC = gin[j].TexC;

		triStream.Append(gout[j]);
	}
}

float4 PS(GeoOut pin) : SV_Target
{
	float4 diffuseAlbedo = gDiffuseMap.Sample(gsamAnisotropicWrap, pin.TexC) * gDiffuseAlbedo;

#ifdef ALPHA_TEST
	clip(diffuseAlbedo.a - 0.1f);
#endif

	// normal vector를 보간하면 normal하지 않아지므로, renormalize 수행
	pin.NormalW = normalize(pin.NormalW);

	// 
	float3 toEyeW = gEyePosW - pin.PosW;
	float distToEye = length(toEyeW);
	toEyeW /= distToEye;

	// Light terms;
	float4 ambient = gAmbientLight * diffuseAlbedo;

	const float shininess = 1.0f - gRoughness;
	Material mat = { diffuseAlbedo, gFresnelR0, shininess };
	float3 shadowFactor = 1.0f;
	float4 directLight = ComputeLighting(gLights, mat, pin.PosW,
		pin.NormalW, toEyeW, shadowFactor);

	float4 litColor = ambient + directLight;

#ifdef FOG
	float fogAmount = saturate((distToEye - gFogStart) / gFogRange);
	litColor = lerp(litColor, gFogColor, fogAmount);
#endif

	litColor.a = diffuseAlbedo.a;

	return litColor;
}