cbuffer cbPerObject : register(b0)
{
	float4x4 gWorld;
};

cbuffer cbPass : register(b1)
{
	float4x4 gView;
	float4x4 gInvView;
	float4x4 gProj;
	float4x4 gInvProj;
	float4x4 gViewProj;
	float4x4 gInvViewProj;
	float3 gEyePosW;
	float cbPerObjectPad1;
	float2 gRenderTargetSize;
	float2 gInvRenderTargetSize;
	float gNearZ;
	float gFarZ;
	float gTotalTime;
	float gDeltatime;
};

struct VertexIn
{
	float3 PosL  : POSITION;
	float4 Color : COLOR;
};

struct VertexOut
{
	float3 PosL  : POSITION;
	float4 Color : COLOR;
};

struct GSOutput
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float4 Color : COLOR;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	vout.PosL = vin.PosL;
	vout.Color = vin.Color;

	return vout;
}

void Subdivide(VertexOut inVerts[3], out VertexOut outVerts[6])
{
	//       v1
	//       *
	//      / \
	//     /   \
	//  m0*-----*m1
	//   / \   / \
	//  /   \ /   \
	// *-----*-----*
	// v0    m2     v2

	VertexOut m[3];

	m[0].PosL = 0.5f * (inVerts[0].PosL + inVerts[1].PosL);
	m[1].PosL = 0.5f * (inVerts[1].PosL + inVerts[2].PosL);
	m[2].PosL = 0.5f * (inVerts[2].PosL + inVerts[0].PosL);

	m[0].Color = 0.5f * (inVerts[0].Color + inVerts[1].Color);
	m[1].Color = 0.5f * (inVerts[1].Color + inVerts[2].Color);
	m[2].Color = 0.5f * (inVerts[2].Color + inVerts[0].Color);

	// 단위 구에 투영한다
	m[0].PosL = normalize(m[0].PosL);
	m[1].PosL = normalize(m[1].PosL);
	m[2].PosL = normalize(m[2].PosL);

	// 법선을 유도한다
	//m[0].normalL = m[0].PosL;
	//m[1].normalL = m[1].PosL;
	//m[2].normalL = m[2].PosL;

	// 텍스처 좌표를 보간한다
	//m[0].Tex = 0.5f * (inVerts[0].Tex + inVerts[1].Tex);
	//m[1].Tex = 0.5f * (inVerts[1].Tex + inVerts[2].Tex);
	//m[2].Tex = 0.5f * (inVerts[2].Tex + inVerts[0].Tex);

	outVerts[0] = inVerts[0];
	outVerts[1] = m[0];
	outVerts[2] = m[2];
	outVerts[3] = m[1];
	outVerts[4] = inVerts[2];
	outVerts[5] = inVerts[1];
}

void OutputSubdivision(VertexOut v[6], inout TriangleStream<GSOutput> triStream)
{
	GSOutput gout[6];

	// unroll 안 넣으면 warning 뜸
	[unroll]
	for (int i = 0; i < 6; ++i)
	{
		float4 posW = mul(float4(v[i].PosL, 1.0f), gWorld);
		gout[i].PosW = posW.xyz;
		// gout[i]NormalW = mul(v[i].NormalL, (float3x3)gWorldInvTranspose);

		gout[i].Color = v[i].Color;

		// 동차 절단 공간으로 변환한다
		gout[i].PosH = mul(posW, gViewProj);

		//gout[i].Tex = v[i].Tex;
	}

	//       v1
	//       *
	//      / \
	//     /   \
	//  m0*-----*m1
	//   / \   / \
	//  /   \ /   \
	// *-----*-----*
	// v0    m2     v2

	// 세분된 삼각형들을 두 개의 띠로 그린다
	// 띠 1 : 아래쪽 삼각형 세 개
	// 띠 2 : 위쪽 삼각형 하나

	triStream.RestartStrip();
	[unroll]
	for (int j = 0; j < 5; ++j)
	{
		triStream.Append(gout[j]);
	}
	triStream.RestartStrip();

	triStream.Append(gout[1]);
	triStream.Append(gout[5]);
	triStream.Append(gout[3]);
}

// maxvertexcount는 정점의 개수가 아니라 TriangleStream의 크기에 맞춰 적어야 한다
// ex) 한 번 SubDivide했을 때 정점의 개수인 6이 아닌
// TriangleStream의 크기(삼각형 띠에 들어가는 index 개수)에 맞춰 8을 적는다
[maxvertexcount(32)]
void GS(
	triangle VertexOut gin[3],
	inout TriangleStream< GSOutput > output
)
{
	float3 centerL = (gin[0].PosL + gin[1].PosL + gin[2].PosL) / 3;
	float3 centerW = mul(float4(centerL, 1.0f), gWorld).xyz;

	float d = distance(centerW, gEyePosW);
	const float d0 = 15.f;
	const float d1 = 30.f;

	int LOD_Level = ceil(2.0f * saturate((d1 - d) / (d1 - d0)));
	if (LOD_Level == 0)
	{
		GSOutput gout[3];

		[unroll]
		for (int i = 0; i < 3; ++i)
		{
			float4 posW = mul(float4(gin[i].PosL, 1.0f), gWorld);
			gout[i].PosW = posW.xyz;
			gout[i].Color = gin[i].Color;

			// 동차 절단 공간으로 변환한다
			gout[i].PosH = mul(posW, gViewProj);
		}

		output.Append(gout[0]);
		output.Append(gout[1]);
		output.Append(gout[2]);
	}
	else 
	{
		VertexOut v[6];
		Subdivide(gin, v);

		if (LOD_Level == 1)
		{
			OutputSubdivision(v, output);
		}
		else
		{
			VertexOut inVerts1[3] = { v[0], v[1], v[2] };
			// 1,2,3으로 하면 감기 순서에 따라 뒷면 삼각형이 됨
			// 시계 방향으로 감도록 2, 1, 3으로 설정해야함
			VertexOut inVerts2[3] = { v[2], v[1], v[3] };
			VertexOut inVerts3[3] = { v[2], v[3], v[4] };
			VertexOut inVerts4[3] = { v[1], v[5], v[3] };

			VertexOut outVerts1[6];
			VertexOut outVerts2[6];
			VertexOut outVerts3[6];
			VertexOut outVerts4[6];

			Subdivide(inVerts1, outVerts1);
			Subdivide(inVerts2, outVerts2);
			Subdivide(inVerts3, outVerts3);
			Subdivide(inVerts4, outVerts4);

			OutputSubdivision(outVerts1, output);
			OutputSubdivision(outVerts2, output);
			OutputSubdivision(outVerts3, output);
			OutputSubdivision(outVerts4, output);
		}
	}
}

float4 PS(GSOutput pin) : SV_TARGET
{
	return pin.Color;
}