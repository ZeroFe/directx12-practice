#ifndef NUM_DIR_LIGHTS
#define NUM_DIR_LIGHTS 3
#endif

#ifndef NUM_POINT_LIGHTS
#define NUM_POINT_LIGHTS 0
#endif

#ifndef NUM_SPOT_LIGHTS
#define NUM_SPOT_LIGHTS 0
#endif

#include "LightingUtil.hlsli"

Texture2D gDiffuseMap : register(t0);

SamplerState gsamPointWrap        : register(s0);
SamplerState gsamPointClamp       : register(s1);
SamplerState gsamLinearWrap       : register(s2);
SamplerState gsamLinearClamp      : register(s3);
SamplerState gsamAnisotropicWrap  : register(s4);
SamplerState gsamAnisotropicClamp : register(s5);

cbuffer cbPerObject : register(b0)
{
	float4x4 gWorld;
	float4x4 gTexTransform;
};

cbuffer cbPass : register(b1)
{
	float4x4 gView;
	float4x4 gInvView;
	float4x4 gProj;
	float4x4 gInvProj;
	float4x4 gViewProj;
	float4x4 gInvViewProj;
	float3 gEyePosW;
	float cbPerObjectPad1;
	float2 gRenderTargetSize;
	float2 gInvRenderTargetSize;
	float gNearZ;
	float gFarZ;
	float gTotalTime;
	float gDeltaTime;
	float4 gAmbientLight;

	float4 gFogColor;
	float gFogStart;
	float gFogRange;
	float2 cbPerObjectPad2;

	Light gLights[MaxLights];
};

cbuffer cbMaterial : register(b2)
{
	float4 gDiffuseAlbedo;
	float3 gFresnelR0;
	float gRoughness;
	float4x4 gMatTransform;
};

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
};

struct GeoOut
{
	float4 PosH : SV_POSITION;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0.0f;

	float4 posW = mul(float4(vin.PosL, 1.0f), gWorld);
	vout.PosW = posW.xyz;

	// nonuniform scaling
	vout.NormalW = mul(vin.NormalL, (float3x3)gWorld);

	vout.PosH = mul(posW, gViewProj);

	return vout;
}

// Normal Vector에 맞게 화살표 그림
[maxvertexcount(5)]
void GS(point VertexOut gin[1],
	inout LineStream<GeoOut> lineStream)
{
	// Normal Vector 화살표의 길이
	const float L = 0.5f;
	float3 lineEnd = gin[0].PosW + L * gin[0].NormalW;

	// Normal Vector가 그린 화살표의 방향을 보여주는 선분 점도 그려야함
	// '<-'-- 이렇게 화살표 두 날개
	float3 look = gEyePosW - lineEnd;
	// 화살표의 방향은 NormalW임
	float3 right = normalize(cross(look, gin[0].NormalW));
	const float sideSize = 0.15f;
	float3 sideUp = sideSize * L * gin[0].NormalW;
	float3 sideRight = sideSize * L * right;
	float3 leftSideEnd = lineEnd - sideUp - sideRight;
	float3 rightSideEnd = lineEnd - sideUp + sideRight;

	// 구한 좌표들로 화살표 만들기
	GeoOut gout[4];
	gout[0].PosH = gin[0].PosH;
	gout[1].PosH = mul(float4(lineEnd, 1.0f), gViewProj);
	gout[2].PosH = mul(float4(leftSideEnd, 1.0f), gViewProj);
	gout[3].PosH = mul(float4(rightSideEnd, 1.0f), gViewProj);

	lineStream.Append(gout[0]);
	lineStream.Append(gout[1]);
	lineStream.Append(gout[2]);
	lineStream.RestartStrip();
	lineStream.Append(gout[1]);
	lineStream.Append(gout[3]);
}

float4 PS(GeoOut pin) : SV_Target
{
	// 화살표는 흰색으로 나타냄
	return float4(1.0f, 1.0f, 1.0f, 1.0f);
}