cbuffer cbPerObject : register(b0)
{
	float4x4 gWorld;
};

cbuffer cbPass : register(b1)
{
	float4x4 gView;
	float4x4 gInvView;
	float4x4 gProj;
	float4x4 gInvProj;
	float4x4 gViewProj;
	float4x4 gInvViewProj;
	float3 gEyePosW;
	float cbPerObjectPad1;
	float2 gRenderTargetSize;
	float2 gInvRenderTargetSize;
	float gNearZ;
	float gFarZ;
	float gTotalTime;
	float gDeltatime;
};

struct VertexIn
{
	float3 PosL  : POSITION;
	float4 Color : COLOR;
};

struct VertexOut
{
	float3 PosL  : POSITION;
	float4 Color : COLOR;
};

struct HullOut
{
	float3 PosL : POSITION;
	float4 Color : COLOR;
};

struct DomainOut
{
	float4 PosH : SV_POSITION;
	float4 Color : COLOR;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	vout.PosL = vin.PosL;
	vout.Color = vin.Color;

	return vout;
}


struct PatchTess
{
	float EdgeTess[3] : SV_TessFactor;
	float InsideTess[1] : SV_InsideTessFactor;
};

PatchTess ConstantHS(InputPatch<VertexOut, 3> patch, uint patchID : SV_PrimitiveID)
{
	PatchTess pt;

	float3 centerL = (patch[0].PosL + patch[1].PosL + patch[2].PosL) / 3;
	float3 centerW = mul(float4(centerL, 1.0f), gWorld).xyz;

	float d = distance(centerW, gEyePosW);

	// patch와 카메라 사이의 거리에 따라 tessellation 정도를 설정
	// [d0, d1]으로 tessellation 정도 설정 가능
	const float d0 = 5.0f;
	const float d1 = 50.0f;
	float tess = 8.0f * saturate((d1 - d) / (d1 - d0));

	pt.EdgeTess[0] = tess;
	pt.EdgeTess[1] = tess;
	pt.EdgeTess[2] = tess;

	pt.InsideTess[0] = tess;

	return pt;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("ConstantHS")]
[maxtessfactor(64.0f)]
HullOut HS(InputPatch<VertexOut, 3> p,
	uint i : SV_OutputControlPointID,
	uint patchId : SV_PrimitiveID)
{
	HullOut hout;

	hout.PosL = p[i].PosL;
	hout.Color = p[i].Color;

	return hout;
}

[domain("tri")]
DomainOut DS(PatchTess patchTess,
	float2 uv : SV_DomainLocation,
	const OutputPatch<HullOut, 3> tri)
{
	//       v1
	//       *
	//      / \
	//     /   \
	//  m0*-----*m1
	//   / \   / \
	//  /   \ /   \
	// *-----*-----*
	// v0    m2     v2

	DomainOut dout;

	// Triangle interpolation
	float3 v1 = (tri[1].PosL - tri[0].PosL) * uv.x;
	float3 v2 = (tri[2].PosL - tri[0].PosL) * uv.y;
	float3 p = tri[0].PosL + v1 + v2;

	float4 c1 = (tri[1].Color - tri[0].Color) * uv.x;
	float4 c2 = (tri[2].Color - tri[0].Color) * uv.y;
	float4 c = tri[0].Color + c1 + c2;

	// 단위 구에 투영한다
	p = normalize(p);

	// 법선을 유도한다
	//float3 n1 = (tri[1].Normal - tri[0].Normal) * uv.x;
	//float3 n2 = (tri[2].Normal - tri[0].Normal) * uv.y;
	//float3 n = tri[0].Normal + n1 + n2;
	// n = normalize(n);

	// 텍스처 좌표를 보간한다
	//m[0].Tex = 0.5f * (inVerts[0].Tex + inVerts[1].Tex);
	//m[1].Tex = 0.5f * (inVerts[1].Tex + inVerts[2].Tex);
	//m[2].Tex = 0.5f * (inVerts[2].Tex + inVerts[0].Tex);

	float4 posW = mul(float4(p, 1.0f), gWorld);
	dout.PosH = mul(posW, gViewProj);
	dout.Color = c;

	return dout;
}

float4 PS(DomainOut pin) : SV_TARGET
{
	return pin.Color;
}